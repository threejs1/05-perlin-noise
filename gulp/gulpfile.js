const gulp = require('gulp');
const browserify = require('browserify');
const watchify = require('watchify'); // сборка с кэшированием, которое ускоряет пересборку
const source = require('vinyl-source-stream');
const tsify = require('tsify');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const fancy_log = require('fancy-log');
const buffer = require('vinyl-buffer');
const babelify = require('babelify');

const watchedBrowserify = 
  watchify(browserify()
  .add('../js/src/Main.ts')
  .plugin(tsify, { 
    noImplicitAny: false, // если true то модули без деклараций не пройдут
    target: 'es6',
  }));

function bundle() {
  return watchedBrowserify
    .transform(babelify, { extensions: [ '.tsx', '.ts', 'js' ] })
    .bundle()
    .pipe(source('bundle.js'))
    .on('error', function (error) { console.error(error.toString()); })
    .pipe(gulp.dest('../js/dist'));
}

gulp.task('default_1', gulp.series(function () {
  return watchedBrowserify
    .plugin(tsify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('../js/dist'));
}));

gulp.task('default', gulp.series(function () {
  return watchedBrowserify
    .transform(babelify, { extensions: [ '.tsx', '.ts', 'js' ] })
    .bundle()
    .pipe(source('bundle.js'))
    .on('error', function (error) { console.error(error.toString()); })
    .pipe(gulp.dest('../js/dist'));
}));


watchedBrowserify.on('update', bundle);
watchedBrowserify.on('log', fancy_log);
