declare var dat: any;

export class Gui {
  private static controls: any = {};
  private static calls: any = [];
  private static gui: any = {};

  public static add(propertyName, defaultValue, params, methods) {
    if (!this.gui) {
      this.calls.push(arguments);
      return;
    }

    //params is methods
    if (typeof params === 'object' && params.constructor !== Array) {
      methods = params;
      params = null;
    }

    this.controls[propertyName] = defaultValue;

    var stringCode = `this.gui.add(this.controls,'${propertyName}'`;

    if (params && params.constructor === Array) {
      stringCode += ',' + params.join(',');
    }

    stringCode += ')';
    var propertyController = eval(stringCode);

    if (methods) {
      for (const methodName in methods) {
        if (!methods.hasOwnProperty(methodName)) {
          continue;
        }

        if (methodName === 'listen') {
          if (methods['listen']) {
            propertyController.listen();
          }

          continue;
        }

        propertyController = propertyController[methodName](methods[methodName]);
      }
    }
  }

  /**
   * set property by name
   * @param { String } name
   * @param { String | Number } value 
   */
  public static set(name: string, value: string | number) {
    this.controls[name] = value;
  }

  /**
   * get property by name
   * @param { String } name
   * @return { String | Number } property value
   */
  public static get(name: string): any {
    return this.controls[name];
  }

  public static init() {
    // this.gui = new dat.GUI();

    // выполнение отложенных вызовов
    // for (let i = 0; i < Gui.calls.length; i++) {
    //   this.add(...this.calls[i]);
    // }
  }
}