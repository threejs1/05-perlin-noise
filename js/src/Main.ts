import { Utils } from "./Utils";
import { PCamera } from "./PCamera";
import * as THREE from 'three';
import * as OrbitControls from './node_modules/three-orbitcontrols/OrbitControls';

export class Main {
  private static time = 0;
  private static uOffste = new THREE.Vector2(0, 0);
  public static stats: any = null;
  public static renderer: THREE.Renderer = null;
  public static scene: THREE.Scene = null;
  public static camera: PCamera = null;
  public static backgroundColor: number = 0x000000;

  private static background() {
    this.scene.background = new THREE.Color(this.backgroundColor);
  }

  private static lighting() {
    const light = new THREE.AmbientLight(0xffffff, 0.6);
    this.scene.add(light);

    const pointLight = new THREE.PointLight(0xffffff, 1, 100);
    pointLight.position.set(-2, 3, -5);
    this.scene.add(pointLight);
  }

  private static setNewSize() {
    const canvas = this.renderer.domElement;
    this.renderer.setSize(canvas.clientWidth, canvas.clientHeight, false);
    this.camera.updateAspectRatio(canvas.clientWidth / canvas.clientHeight);
  }

  public static render() {
    this.stats.update();
    this.renderer.render(this.scene, this.camera);
    this.time += 0.001;
    this.uOffste.x += 0.01;
    this.uOffste.y += 0.01;

    this.scene.traverse(function(child) {
      if (
        child instanceof THREE.Mesh &&
        child.material.type === 'ShaderMaterial'
      ) {
        child.material.uniforms.uTime.value = Main.time;
        child.material.uniforms.uOffste.value = Main.uOffste;
        child.material.needsUpdate = true;
      }
    });

    requestAnimationFrame(function () {
      Main.render();
    });
  }

  public static init() {
    this.stats = Utils.initStats();
    this.renderer = Utils.initRenderer();
    document.getElementById("webgl-output").appendChild(this.renderer.domElement);
    this.scene = new THREE.Scene();
    this.camera = new PCamera(new THREE.Vector3(4.827195168690281, 4.309757224637014, 5.111181807377061));

    const controls = new OrbitControls(this.camera, this.renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.enableZoom = false;

    this.background();
    this.lighting();

    var geometry = new THREE.BoxGeometry(2, 2, 2);
    var material = new THREE.ShaderMaterial({
      fragmentShader: document.getElementById('perlin-fragment-shader').textContent,
      vertexShader: document.getElementById('perlin-vertex-shader').textContent,
      uniforms: {
        uResolution: {
          value: new THREE.Vector2(1920, 1080),
        },
        uTime: {
          value: this.time,
        },
        uOffste: {
          value: this.uOffste,
        },
      },
    });

    var cube = new THREE.Mesh(geometry, material);
    this.scene.add(cube);

    this.render();
    this.camera.lookAt(cube.position);
    window.addEventListener('resize', function () {
      Main.setNewSize();
    });
  }
}

window.onload = function () {
  Main.init();
}
