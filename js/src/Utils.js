"use strict";
exports.__esModule = true;
exports.Utils = void 0;
var Main_1 = require("./Main");
var Utils = /** @class */ (function () {
    function Utils() {
    }
    /**
     * Initialize the statistics domelement
     *
     * @param {Number} type 0: fps, 1: ms, 2: mb, 3+: custom
     * @returns stats javascript object
     */
    Utils.initStats = function (type) {
        var panelType = (typeof type !== 'undefined' && type) && (!isNaN(type)) ? parseInt(type) : 0;
        var stats = new Stats();
        stats.showPanel(panelType); // 0: fps, 1: ms, 2: mb, 3+: custom
        document.body.appendChild(stats.dom);
        return stats;
    };
    Utils.initRenderer = function (additionalProperties) {
        var props = (typeof additionalProperties !== 'undefined' && additionalProperties) ? additionalProperties : {};
        var renderer = new THREE.WebGLRenderer(props);
        renderer.shadowMap.enabled = true;
        renderer.shadowMapSoft = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        renderer.setClearColor(new THREE.Color(Main_1.Main.backgroundColor));
        renderer.setSize(window.innerWidth, window.innerHeight, false);
        renderer.shadowMap.enabled = true;
        document.getElementById("webgl-output").appendChild(renderer.domElement);
        return renderer;
    };
    return Utils;
}());
exports.Utils = Utils;
