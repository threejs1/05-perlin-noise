const SpaceTexture = [
  "// from: https://www.shadertoy.com/view/ltjSWV",
  "//-------------------------Noise--------------------------",
  "// IQ's noise",
  "float pn( in vec3 p )",
  "{",
  "  vec3 ip = floor(p);",
  "  p = fract(p);",
  "  p *= p*(3.0-2.0*p);",
  "  vec2 uv = (ip.xy+vec2(37.0,17.0)*ip.z) + p.xy;",
  "  uv = texture( iChannel0, (uv+ 0.5)/256.0, -100.0 ).yx;",
  "  return mix( uv.x, uv.y, p.z );",
  "}",

  "void mainImage( out vec4 fragColor, in vec2 fragCoord )",
  "{",
  "  vec3 rd = normalize(vec3((gl_FragCoord.xy-0.5*iResolution.xy)/iResolution.y, 1.));",

  "  vec3 stars = vec3(pn(rd*300.0)*0.5+0.5);",
  "  vec3 col = vec3(0.0);",
  "  col = mix(col, vec3(0.8,0.9,1.0), smoothstep(0.98, 1.0, stars)*clamp(dot(vec3(0.0),rd)+0.75,0.0,1.0));",
  "  col = clamp(col, 0.0, 1.0);",


  "  fragColor = vec4(col, 1.0);",
  "}",
].join("\n");

export { SpaceTexture };
