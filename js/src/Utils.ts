import { Main } from "./Main";

declare var Stats: any;
declare var THREE: any;

export class Utils {
  /**
   * Initialize the statistics domelement
   * 
   * @param {Number} type 0: fps, 1: ms, 2: mb, 3+: custom
   * @returns stats javascript object
   */
  public static initStats(type?: any): Object {
    var panelType = (typeof type !== 'undefined' && type) && (!isNaN(type)) ? parseInt(type) : 0;
    var stats = new Stats();

    stats.showPanel(panelType); // 0: fps, 1: ms, 2: mb, 3+: custom
    document.body.appendChild(stats.dom);

    return stats;
  }

  public static initRenderer(additionalProperties?: any) {
    var props = (typeof additionalProperties !== 'undefined' && additionalProperties) ? additionalProperties : {};
    var renderer = new THREE.WebGLRenderer(props);
    renderer.shadowMap.enabled = true;
    renderer.shadowMapSoft = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    renderer.setClearColor(new THREE.Color(Main.backgroundColor));
    renderer.setSize(window.innerWidth, window.innerHeight, false);
    renderer.shadowMap.enabled = true;
    document.getElementById("webgl-output").appendChild(renderer.domElement);

    return renderer;
  }
}
