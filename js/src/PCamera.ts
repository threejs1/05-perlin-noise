import * as THREE from 'three';
import { Vector3 } from 'three';

/**
 * shell class for THREE.PerspectiveCamera
 * 
 * @param {THREE.Vector3} [initialPosition]
*/
export class PCamera extends THREE.PerspectiveCamera {
  aspect: number;
  
  public updateAspectRatio(aspect: number) {
    this.aspect = aspect;
    this.updateProjectionMatrix();
  }

  constructor(initialPosition?: Vector3) {
    super(45, window.innerWidth / window.innerHeight, 0.1, 1000);
    this.position.copy(initialPosition || new THREE.Vector3(0, -10, 0));
    this.lookAt(new THREE.Vector3(0, 0, 0));
  }
}
